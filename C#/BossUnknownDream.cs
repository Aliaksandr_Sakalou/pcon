﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossUnknownDream : Enemy
{
    float dreamTimer;

    public override void Start()
    {
        level = 3;
        xpReward = 0;
        jpReward = 0;
        types = Definition.EnemyTypes.BOSSUNKNOWNDREAM;
        monsterName = "Unknown Dream";
        dreamTimer = 60;
        //array indexes of each stat:
        //STR - 0
        //SPD - 1
        //DEX - 2
        //CON - 3
        //INT - 4
        //WIS - 5
        //LUK - 6
        enemyStats = new int[] { 12, 5, 7, 10, 1, 1, 1 };
        hpStats = 1000;
        maxhpStats = hpStats;
        randomNumber = UnityEngine.Random.Range(0, 101);
        weaponDamage = 3;
        coolDownAttack = Random.Range(0, 6);
    }

    public override void Update()
    {
        dreamTimer -= Time.deltaTime;
    }

    public override void Attack()
    {
        if (coolDownAttack > 6)
        {
            randomAttackSkill = UnityEngine.Random.Range(1, 6);
            if (randomAttackSkill == 2)
            {
                SingleStab();
            }
            else if (randomAttackSkill == 3)
            {
                SwingAttack();
            }
            else if (randomAttackSkill == 4)
            {
                Wail();
            }
            else if (HP==0)
            {
                Awakening();
            }
            else NormalAttack();
            coolDownAttack = 0;
        }
    }

    void SingleStab()
    {
        NormalAttack();
        charactersList[orderCharacterAttack].SetBleedStatus(true);
    }

    void SwingAttack()
    {        
        foreach (Character characterToAttack in charactersList)
        {
            currentTarget = characterToAttack;
            NormalAttack();
            randomAttackSkill = UnityEngine.Random.Range(1, 6);
            if (randomAttackSkill < 4) characterToAttack.SetBleedStatus(true); 
        }                  
    }

    void Wail()
    {        
        foreach (Character characterToAttack in charactersList)
        {
            currentTarget = characterToAttack;
            NormalAttack();
            currentTarget.AP-=2;
        }
    }

    void Awakening()
    {
        foreach (Character characterToAttack in charactersList)
        {
            if (characterToAttack.GetCharacterName == "Hero") characterToAttack.HP -= 9999;
        }
    }

}
