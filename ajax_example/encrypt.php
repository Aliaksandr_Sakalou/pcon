<?php

function encryption($s)
{
    $length = strlen($s);
    $columns = ceil(sqrt($length));
    $rows = floor(sqrt($length));
    $result = "";
    
    for ($i = 0; $i < $columns; $i ++) {
        for ($j = (int) $i; $j < $length; $j += $columns) {            
            $result .= ($s[(int) $j]);
        }
        $result .= (" ");
    }
    
    return $result;
}

$s = $_POST['msg1'];

echo encryption($s);